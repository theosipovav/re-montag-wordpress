 <!DOCTYPE html>
 <html lang="ru">

 <head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160717593-1"></script>
   <script>
     window.dataLayer = window.dataLayer || [];

     function gtag() {
       dataLayer.push(arguments);
     }
     gtag('js', new Date());

     gtag('config', 'UA-160717593-1');
   </script>

   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <meta name="msapplication-TileColor" content="#1b212d">
   <meta name="theme-color" content="#1b212d">
   <meta name="facebook-domain-verification" content="8gmwwed5lbmqa4guqjwkskjp9c53fj" />
   <title><?php wp_title('—', true, 'right'); ?><?php bloginfo('name'); ?></title>
   <?php wp_head(); ?>

 <body>
   <nav class="nav">
     <div class="container">
       <a href="/" class="nav__logo">
         <svg>
           <use xlink:href="#logo"></use>
         </svg>
       </a>
       <? if (!is_page(6) && !is_page(504)) : ?>
         <div class="nav__tagline">
           Напольные покрытия<br />
           продажа и укладка
         </div>
       <? endif; ?>
       <div class="nav__contacts">
         <? if (get_theme_mod('setting-phone')) : ?>
           <a href="tel: <? echo get_theme_mod('setting-phone'); ?>" class="nav__contacts__phone">
             <? echo get_theme_mod('setting-phone'); ?>
           </a>
         <? endif; ?>
         <? if (get_theme_mod('setting-email')) : ?>
           <a href="mailto: <? echo get_theme_mod('setting-email'); ?>" class="nav__contacts__email">
             <? echo get_theme_mod('setting-email'); ?>
           </a>
         <? endif; ?>
       </div>
     </div>
   </nav>