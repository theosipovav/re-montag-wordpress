<section class="section almazbur-price">
    <input type="hidden" id="UrlAdminAjax" value="<?= admin_url("admin-ajax.php") ?>">
    <div class="container">
        <div class="d-flex flex-column">
            <h2 class="section-title almazbur-price-title"><?= $dataSection['title'] ?></h2>
            <div class="almazbur-price-table-container">
                <table class="almazbur-price-table">
                    <thead>
                        <tr>
                            <th>Диаметр отверстия (мм)</th>
                            <th>Кирпич</th>
                            <th>Неармированный бетон</th>
                            <th>Армированный бетон</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataSection['table'] as $key => $row) : ?>
                            <tr>
                                <td><?= $row['d'] ?></td>
                                <td><?= $row['k'] ?></td>
                                <td><?= $row['nb'] ?></td>
                                <td><?= $row['ab'] ?></td>
                            </tr>

                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
            <div class="almazbur-price-desc">
                <?php foreach ($dataSection['desc'] as $key => $desc) : ?>
                    <div class="d-flex">
                        <span class="almazbur-price--marker">&bull;</span>
                        <p class="almazbur-price--p"><?= $desc['text'] ?></p>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="form-calc-almazbur-container mt-3">
            <div class="d-flex flex-column flex-grow-1 pr-2 pl-2">
                <div class="form-calc-almazbur-card-params">
                    <div class="form-calc-almazbur-card-params--header"><span style="color: #29afe9;">01</span><span style="color: #29afe9;">/</span><span>Выберите материал:</span></div>
                    <div class="form-calc-almazbur-row almazbur-calculate-container justify-content-center pt-3 pb-3">
                        <div class="form-calculate-almazbur--select">
                            <select name="material" class="form-control">
                                <option disabled selected>Выберите материал</option>
                                <option value="k">Кирпич</option>
                                <option value="ab">Армированный бетон</option>
                                <option value="nb">Неармированный бетон</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-calc-almazbur-card-params">
                    <div class="form-calc-almazbur-card-params--header"><span style="color: #29afe9;">02</span><span style="color: #29afe9;">/</span><span>Параметры:</span></div>
                    <div class="form-calc-almazbur-col align-items-center almazbur-calculate-container">
                        <div class="form-group form-group-row">
                            <input type="number" id="InputFormAlmazburDiameter" class="form-control form-calc-almazbur-card-control" name="diameter" min="0" placeholder="Диаметр отверстия" title="Диаметр отверстия, мм">
                            <p class="form-calc-almazbur-card-control-info">мм</p>
                        </div>
                        <div class="form-group form-group-row">
                            <input type="number" id="InputFormAlmazburDepth" class="form-control form-calc-almazbur-card-control" name="depth" min="0" placeholder="Глубина отверстия" title="Глубина отверстия, см">
                            <p class="form-calc-almazbur-card-control-info">см</p>
                        </div>
                        <div class="form-group form-group-row">
                            <input type="number" id="InputFormAlmazburCount" class="form-control form-calc-almazbur-card-control" name="number" min="0" placeholder="Количество отверстий" title="Количество отверстий">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-calc-almazbur-feedback">
                <?php if (true) : ?>
                    <div class="form-calc-almazbur-title">Ориентировочная стоимость работы:</div>
                    <div class="form-calc-almazbur-result">
                        <div class="form-calc-almazbur-error"></div>
                        <div class="form-calc-almazbur-result-value">
                            <span class="form-calc-almazbur-result-price">0</span><span>руб.</span>
                        </div>
                    </div>
                    <div class="form-calc-almazbur-desc">Оставьте заявку прямо сейчас и получите <span>скидку 10%</span></div>
                    <div class="form-calc-almazbur-col align-items-center pt-3">
                        <input type="hidden" name="hidden_price">
                        <input type="hidden" name="hidden_material">
                        <input type="hidden" name="hidden_diameter">
                        <input type="hidden" name="hidden_depth">
                        <input type="hidden" name="hidden_number">
                        <input type="text" name="name" id="" class="form-control" placeholder="Ваше имя" required>
                        <input type="tel" name="tel" id="" class="form-control" placeholder="Номер телефона" required>
                        <div class="d-flex justify-content-center align-items-center flex-grow-1">
                            <button class="btn-intro-v2 mt-2" type="submit">
                                <span>Свяжитесь со мной</span>
                            </button>
                        </div>
                    </div>
                <?php else : ?>
                    <?= do_shortcode($dataSection['code']) ?>
                <?php endif ?>
            </div>

        </div>
    </div>
</section>