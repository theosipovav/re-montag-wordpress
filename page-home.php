<?php
/*
Template Name: Home
*/
get_header();
?>
<header class="home-header">
    <div class="container">
        <h1 class="home-header__title">
            <span>РЕ-МОНТАЖ</span> -<br />
            строительно-ремонтная<br />
            компания <span>в Пензе</span>
        </h1>
        <div class="home-header__sub">10 лет ремонтируем и строим коммерческую недвижимость</div>
        <div class="home-header__scroll scrollTo" data-target="home-catalog">
            <svg>
                <use xlink:href="#arrow-down"></use>
            </svg>
        </div>
    </div>
</header>
<section class="home-catalog" id="catalog">
    <div class="container">
        <div class="home-catalog__list">
            <div class="home-catalog__list__item">
                <a href="http://remont.re-montag.ru/" target="_blank" class="">
                    <div class="home-catalog__list__image">
                        <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-catalog1.png" alt="">
                    </div>
                    <div class="home-catalog__list__bottom">
                        <div class="home-catalog__list__title">
                            Ремонт и строительство<br />
                            коммерческой недвижимости
                        </div>
                        <a href="http://remont.re-montag.ru/" target="_blank" class="home-catalog__list__button">
                            Подробнее
                            <svg>
                                <use xlink:href="#button-arrow"></use>
                            </svg>
                        </a>
                    </div>
                </a>
            </div><!-- home-catalog__list__item -->
            <div class="home-catalog__list__item">
                <a href="http://c.re-montag.ru/" target="_blank" class="">
                    <div class="home-catalog__list__image">
                        <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-catalog2.png" alt="">
                    </div>
                    <div class="home-catalog__list__bottom">
                        <div class="home-catalog__list__title">
                            Продажа, установка и сервисное<br />
                            обслуживание кондиционеров
                        </div>
                        <a href="http://c.re-montag.ru/" target="_blank" class="home-catalog__list__button">
                            Подробнее
                            <svg>
                                <use xlink:href="#button-arrow"></use>
                            </svg>
                        </a>
                    </div>
                </a>
            </div><!-- home-catalog__list__item -->
            <div class="home-catalog__list__item">
                <a href="/almaz-bur" class="">
                    <div class="home-catalog__list__image">
                        <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-catalog5.png" alt="">
                    </div>
                    <div class="home-catalog__list__bottom">
                        <div class="home-catalog__list__title">
                            Алмазное бурение:<br />
                            бурение и резка профессиональным оборудованием в Пензе
                        </div>
                        <a href="/almaz-bur" class="home-catalog__list__button">
                            Подробнее
                            <svg>
                                <use xlink:href="#button-arrow"></use>
                            </svg>
                        </a>
                    </div>
                </a>
            </div><!-- home-catalog__list__item -->
            <div class="home-catalog__list__item">
                <a href="http://mr.re-montag.ru/" target="_blank" class="">
                    <div class="home-catalog__list__image">
                        <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-catalog4.png" alt="">
                    </div>
                    <div class="home-catalog__list__bottom">
                        <div class="home-catalog__list__title">
                            Механизированные работы:<br />
                            штукатурка, покраска, шпаклёвка
                        </div>
                        <a href="http://mr.re-montag.ru/" target="_blank" class="home-catalog__list__button">
                            Подробнее
                            <svg>
                                <use xlink:href="#button-arrow"></use>
                            </svg>
                        </a>
                    </div>
                </a>
            </div><!-- home-catalog__list__item -->
        </div>
    </div>
</section>
<section class="home-numbers">
    <div class="container">
        <div class="home-numbers__title">ООО «РЕ-МОНТАЖ» в цифрах</div>
        <div class="home-numbers__sub">
            Среднегодовой объем строительных, ремонтных и монтажных
            работ — более 10 000 м²
        </div>
        <div class="home-numbers__list">
            <div class="home-numbers__list__item">
                <div class="home-numbers__list__number">
                    20
                </div>
                <div class="home-numbers__list__desc">
                    построено свыше<br />
                    20 объектов
                </div>
            </div>
            <div class="home-numbers__list__item">
                <div class="home-numbers__list__number">
                    250
                </div>
                <div class="home-numbers__list__desc">
                    отремонтировано<br />
                    250 объектов
                </div>
            </div>
            <div class="home-numbers__list__item">
                <div class="home-numbers__list__number">
                    40
                </div>
                <div class="home-numbers__list__desc">
                    спроектировано<br />
                    свыше 40 проектов
                </div>
            </div>
            <div class="home-numbers__list__item">
                <div class="home-numbers__list__number">
                    10
                </div>
                <div class="home-numbers__list__desc">
                    более 10 лет в<br />
                    строительстве
                </div>
            </div>
        </div>
        <div class="home-numbers__note">
            ООО «РЕ-МОНТАЖ» выполняет общестроительные и специальные работы по возведению зданий,
            ремонту сооружений, производственных комплексов и складов любой сложности, включая полный
            перечень работ по строительству зданий «с нуля».
        </div>
    </div>
</section>
<section class="home-service" id="design">
    <div class="container">
        <div class="home-service__title">Услуги проектирования и дизайна</div>
        <div class="home-service__sub">
            РЕ-МОНТАЖ оказывает услуги дизайна интерьера коммерческих объектов:
            офисов, медицинских центров, магазинов, ресторанов и кафе. Недорого
            выполним дизайн проект и в короткие сроки.
        </div>
        <div class="home-service__cons">
            <div class="home-service__cons__content">
                <div class="home-service__cons__title">
                    Мы предлагаем полный<br />
                    комплекс услуг:
                </div>
                <div class="home-service__cons__list">
                    <div class="home-service__cons__list__item">
                        • Обмерный план
                    </div>
                    <div class="home-service__cons__list__item">
                        • Техническое задание
                    </div>
                    <div class="home-service__cons__list__item">
                        • Планировочное решение
                    </div>
                    <div class="home-service__cons__list__item">
                        • 3D визуализация
                    </div>
                    <div class="home-service__cons__list__item">
                        • Чертежи рабочей документации
                    </div>
                    <div class="home-service__cons__list__item">
                        • Подбор мебели и материалов
                    </div>
                    <div class="home-service__cons__list__item">
                        • Авторский надзор
                    </div>
                </div>
                <div class="home-service__cons__note">
                    Наша команда «РЕ-МОНТАЖ» поможет вам определиться со стилевым направлением, планировкой и многими другими важными моментами.
                </div>
            </div>
            <a href="" class="home-service__cons__button toModal" data-modal="modal-typical" data-modal-title="Получить консультацию дизайнера" data-modal-ya="dizain">
                <span>Получить консультацию дизайнера</span>
            </a>
        </div>
        <div class="home-service__list">
            <div class="home-service__list__item">
                <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-service-item1.png" alt="">
            </div>
            <div class="home-service__list__item">
                <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-service-item2.png" alt="">
            </div>
            <div class="home-service__list__item">
                <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-service-item3.png" alt="">
            </div>
        </div>
    </div>
</section>
<? $slider = get_field('slider'); ?>
<? $index = 1; ?>
<section class="home-projects" id="projects">
    <div class="container">
        <div class="home-projects__title">Выполненные проекты РЕ-МОНТАЖ</div>
        <div class="home-projects__list">
            <? foreach ($slider as $slide) : ?>
            <div class="home-projects__list__item">
                <div class="home-projects__list__title">
                    <? echo $slide['description']; ?>
                </div>
                <div class="home-projects__list__slider">
                    <div class="home-projects__list__slider__inner owl-carousel">
                        <? foreach ($slide['items'] as $item) : ?>
                        <a href="<? echo gi($item['image'], 'full'); ?>" data-fancybox="slider<? echo $index; ?>" class="home-projects__list__slider__item" data-fancybox>
                            <img src="<? echo gi($item['image'], 'slider_img'); ?>" alt="">
                        </a>
                        <? endforeach; ?>
                    </div>
                    <div class="home-projects__list__slider__controls">
                        <div class="home-projects__list__slider__control">
                            <svg>
                                <use xlink:href="#slider-left"></use>
                            </svg>
                        </div>
                        <div class="home-projects__list__slider__control">
                            <svg>
                                <use xlink:href="#slider-right"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="home-projects__list__slider__dots">
                        <button></button>
                        <button class="active"></button>
                        <button></button>
                    </div>
                </div>
            </div>
            <? $index++; ?>
            <? endforeach; ?>
        </div>
    </div>
</section>
<section class="home-clients">
    <div class="container">
        <div class="home-clients__title">Наши клиенты</div>
        <div class="home-clients__sub">
            Наши клиенты - это крупные федеральные компании и местный
            бизнес. Розничные продуктовые сети, банки, магазины известных
            брендов
        </div>
        <div class="home-clients__list">
            <div class="home-clients__list__item">
                <div class="home-clients__list__image">
                    <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-client1.png" alt="">
                </div>
                <div class="home-clients__list__title">
                    Салоны дверей
                </div>
            </div>
            <div class="home-clients__list__item">
                <div class="home-clients__list__image">
                    <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-client2.png" alt="">
                </div>
                <div class="home-clients__list__title">
                    Букмекерская контора
                </div>
            </div>
            <div class="home-clients__list__item">
                <div class="home-clients__list__image">
                    <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-client3.png" alt="">
                </div>
                <div class="home-clients__list__title">
                    Букмекерская контора
                </div>
            </div>
            <div class="home-clients__list__item">
                <div class="home-clients__list__image">
                    <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-client4.png" alt="">
                </div>
                <div class="home-clients__list__title">
                    Салоны дверей
                </div>
            </div>
            <div class="home-clients__list__item">
                <div class="home-clients__list__image">
                    <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-client5.png" alt="">
                </div>
                <div class="home-clients__list__title">
                    Розничные магазины
                </div>
            </div>
            <div class="home-clients__list__item">
                <div class="home-clients__list__image">
                    <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-client6.png" alt="">
                </div>
                <div class="home-clients__list__title">
                    Розничные магазины
                </div>
            </div>
            <div class="home-clients__list__item">
                <div class="home-clients__list__image">
                    <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-client7.png" alt="">
                </div>
                <div class="home-clients__list__title">
                    Розничные магазины
                </div>
            </div>
            <div class="home-clients__list__item">
                <div class="home-clients__list__image">
                    <img src="<? echo get_template_directory_uri(); ?>/dist/img/home-client8.png" alt="">
                </div>
                <div class="home-clients__list__title">
                    Фирменые магазины
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>