<?php
$sectionAlmazburForm = get_field('form', 504);
?>

<section class="contacts" id="contacts">
  <div class="container">
    <div class="contacts__title">
      Контакты
    </div>
    <div class="contacts__inner">
      <div class="contacts__content">
        <div class="contacts__list">
          <div class="contacts__list__item">
            <div class="contacts__list__title">Пенза</div>
            <div class="contacts__list__desc">
              г. Пенза, ул. Измайлова, 17а
            </div>
            <? if (get_theme_mod('setting-phone')): ?>
            <a href="tel: <? echo get_theme_mod('setting-phone'); ?>" class="contacts__list__phone">
              <? echo get_theme_mod('setting-phone'); ?>
            </a>
            <? endif; ?>
            <? if (get_theme_mod('setting-email')): ?>
            <a href="mailto: <? echo get_theme_mod('setting-email'); ?>" class="contacts__list__link">
              <? echo get_theme_mod('setting-email'); ?>
            </a>
            <? endif; ?>
          </div>
          <div class="contacts__list__item">
            <div class="contacts__list__title">Заречный</div>
            <div class="contacts__list__desc">
              Пензенская область, г. Заречный,<br />
              ул. Индустриальная, д.8
            </div>
          </div>
          <div class="contacts__list__item">
            <div class="contacts__list__title">Контакты директора:</div>
            <a href="tel: +7 (8412) 71-19-00" class="contacts__list__phone">
              +7 (8412) 71-19-00
            </a>
            <a href="mailto: director@re-montag.ru" class="contacts__list__link">director@re-montag.ru</a>
          </div>
        </div>
        <div class="contacts__socials">
          <? if (get_theme_mod('setting-inst')): ?>
          <a href="<? echo get_theme_mod('setting-inst'); ?>" target="_blank" class="contacts__socials__item contacts__socials__item-inst">
            <svg>
              <use xlink:href="#icon-inst"></use>
            </svg>
          </a>
          <? endif; ?>
          <? if (get_theme_mod('setting-you')): ?>
          <a href="<? echo get_theme_mod('setting-you'); ?>" target="_blank" class="contacts__socials__item contacts__socials__item-you">
            <svg>
              <use xlink:href="#icon-you"></use>
            </svg>
          </a>
          <? endif; ?>
        </div>
      </div>
      <div class="contacts__map" id="map"></div>
    </div>
  </div>
</section>
<footer class="footer">
  <div class="container">
    <a href="/" class="footer__logo">
      <svg>
        <use xlink:href="#logo"></use>
      </svg>
    </a>
    <div class="footer__links">
      <a href="http://remont.re-montag.ru/" target="_blank" class="footer__links__item">Ремонт и строительство</a>
      <a href="http://c.re-montag.ru/" target="_blank" class="footer__links__item">Кондиционеры</a>
      <a href="/catalog" class="footer__links__item">Напольные покрытия</a>
      <a href="" class="footer__links__item">Механизированные работы</a>
    </div>
  </div>
</footer>
<div class="modal modal-typical">
  <div class="modal__fade"></div>
  <div class="modal__inner">
    <div class="modal__content">
      <div class="modal__close"></div>
      <div class="modal__title">Получить консультацию</div>
      <?= do_shortcode('[contact-form-7 id="210" title="Modal form"]'); ?>
    </div>
  </div>
</div>

<div class="modal modal-almazbur">
  <div class="modal__fade"></div>
  <div class="modal__inner">
    <div class="modal__content">
      <div class="modal__close"></div>
      <div class="modal__title"><?= $sectionAlmazburForm['title'] ?></div>
      <?= do_shortcode($sectionAlmazburForm['code']); ?>
    </div>
  </div>
</div>

<div id="thanks" class="modal modal-thanks">
  <div class="modal__fade"></div>
  <div class="modal__inner">
    <div class="modal__content">
      <div class="modal__bg"></div>
      <div class="modal__close"></div>
      <div class="modal__title">Спасибо!</div>
      <div class="modal__sub">
        Ваша заявка успешно отправлена. Мы свяжемся
        с вами в ближайшее время.
      </div>
    </div>
  </div>
</div>
<div class="menu__back"></div>
<div class="menu__bg"></div>
<div class="menu">
  <div class="menu__close"></div>
  <div class="menu__open">
    <div class="menu__open__bar"></div>
    <div class="menu__open__bar"></div>
    <div class="menu__open__bar"></div>
  </div>
  <div class="menu__inner">
    <div class="menu__list">
      <a href="/" class="menu__list__item">Главная</a>
      <? if(is_page(6)): ?>
      <a href="/#catalog" data-target="home-catalog" class="menu__list__item scrollTo">Услуги</a>
      <a href="/#design" data-target="home-service" class="menu__list__item scrollTo">Дизайн-проект</a>
      <a href="/#projects" class="menu__list__item scrollTo">Выполненные проекты</a>
      <? else: ?>
      <a href="<? the_permalink(11); ?>" class="menu__list__item">Ламинат</a>
      <a href="<? the_permalink(37); ?>" class="menu__list__item">Паркет</a>
      <a href="<? the_permalink(49); ?>" class="menu__list__item">ПВХ-плитка</a>
      <a href="<? the_permalink(55); ?>" class="menu__list__item">Пробковый пол</a>
      <? endif; ?>
      <a href="/#contacts" data-target="contacts" class="menu__list__item scrollTo">Контакты</a>
    </div>
    <div class="menu__contacts">
      <a href="" class="menu__contacts__button toModal" data-modal="modal-typical" data-modal-title="Получить консультацию" data-modal-ya="konsult">Получить консультацию</a>
      <? if (get_theme_mod('setting-phone')): ?>
      <a href="tel: <? echo get_theme_mod('setting-phone'); ?>" class="menu__contacts__phone">
        <? echo get_theme_mod('setting-phone'); ?>
      </a>
      <? endif; ?>
      <? if (get_theme_mod('setting-email')): ?>
      <a href="mailto: <? echo get_theme_mod('setting-email'); ?>" class="menu__contacts__email">
        <? echo get_theme_mod('setting-email'); ?>
      </a>
      <? endif; ?>
    </div>
  </div>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
  (function(m, e, t, r, i, k, a) {
    m[i] = m[i] || function() {
      (m[i].a = m[i].a || []).push(arguments)
    };
    m[i].l = 1 * new Date();
    k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
  })
  (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

  ym(56319664, "init", {
    clickmap: true,
    trackLinks: true,
    accurateTrackBounce: true,
    webvisor: true
  });
</script>
<noscript>
  <div><img src="https://mc.yandex.ru/watch/56319664" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<?php
wp_footer();
?>
</body>

</html>