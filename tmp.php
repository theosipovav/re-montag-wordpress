<div class="free__form__note mt-3">Заполните поля формы и мы отправим Вам индивидуальные цены и сроки поставки</div>
<div class="form-free-zamer-almazbur-group">
    <input type="text" name="form_name" class="form-control" placeholder="Ваше имя">
    <input type="tel" name="form_tel" class="form-control" placeholder="Контактный телефон">
</div>
<div class="form-free-zamer-almazbur-group">
    <input type="email" name="" class="form-control" placeholder="Контактный Email">
</div>
<div class="free__form__buttons mt-3">
    <button class="free__form__button"><span>Отправить заявку</span></button>
    <input type="hidden" name="form_target" value="zamer">
    <div class="free__form__contacts">
        <a href="tel: +7 (8412) 22-12-00" class="free__form__phone">+7 (8412) 22-12-00</a>
        <a href="mailto: director@re-montag.ru" class="free__form__email">director@re-montag.ru</a>
    </div>
</div>