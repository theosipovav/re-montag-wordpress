<?php
/*
Template Name: Typical
*/
get_header();
$fields = get_fields();
$catalog = get_field('catalog');
$modal_title = get_field('modal_title');
$modal_catalog_title = get_field('modal_catalog_title');
$modal_target = get_field('modal_target');
$modal_catalog_target = get_field('modal_catalog_target');
?>
<header class="catalog-header" style="background: url(<? echo gi($fields['header_bg'], 'full'); ?>) no-repeat center; background-size: cover">
 <div class="container">
  <h1 class="catalog-header__title">
   <? echo $fields['title']; ?>
  </h1>
  <div class="catalog-header__brands">
   <? foreach ($fields['brands'] as $item): ?>
   <div class="catalog-header__brands__item">
    <img src="<? echo gi($item['item'], 'brand_img'); ?>" alt="">
   </div>
   <? endforeach; ?>
  </div>
  <div class="catalog-header__bottom">
   <a href="" class="catalog-header__button toModal" data-modal="modal-typical" data-modal-title="<? echo $modal_title; ?>" data-modal-ya="<? echo $modal_target; ?>">
    <span>Рассчитать стоимость</span>
   </a>
   <div class="catalog-header__note">
    <? echo $fields['button_text']; ?>
   </div>
  </div>
  <div class="catalog-header__list">
   <div class="catalog-header__list__item">
    <div class="catalog-header__list__icon">
     <svg>
      <use xlink:href="#funds"></use>
     </svg>
    </div>
    <div class="catalog-header__list__text">
     Цена на 25-30%<br />
     ниже чем в магазинах<br />
     стройматериалов<br />
     по городу
    </div>
   </div>
   <div class="catalog-header__list__item">
    <div class="catalog-header__list__icon">
     <svg>
      <use xlink:href="#verification-of-delivery-list-clipboard-symbol"></use>
     </svg>
    </div>
    <div class="catalog-header__list__text">
     Бесплатная доставка<br />
     по Пензе и подъем<br />
     на этаж
    </div>
   </div>
   <div class="catalog-header__list__item">
    <div class="catalog-header__list__icon">
     <svg>
      <use xlink:href="#surface1"></use>
     </svg>
    </div>
    <div class="catalog-header__list__text">
     Гарантия низкой<br />
     цены
    </div>
   </div>
   <div class="catalog-header__list__item">
    <div class="catalog-header__list__icon">
     <svg>
      <use xlink:href="#wrench"></use>
     </svg>
    </div>
    <div class="catalog-header__list__text">
     Известные<br />
     производители из<br />
     Германии, России,<br />
     Бельгии
    </div>
   </div>
  </div>
 </div>
</header>
<section class="catalog-content">
 <div class="container">
  <div class="catalog-content__top">
   <div class="catalog-content__title"><? echo $fields['catalog_title']; ?></div>
   <div class="catalog-content__brands">
   <? foreach ($fields['brands'] as $item): ?>
    <div class="catalog-content__brands__item"><img src="<? echo gi($item['item'], 'brand_img'); ?>" alt=""></div>
<? endforeach; ?>
   </div>
  </div>
   <? if(true): ?>
  <div class="catalog-content__cats">
  <? foreach ($catalog as $key => $items): ?>
   <a href="" data-tab="<? echo $items['title']; ?>" class="catalog-content__cats__item <? echo ($key == 0 ? 'active' : ''); ?>"><? echo $items['title']; ?></a>
  <? endforeach; ?>
  </div>
   <? endif; ?>
  <div class="catalog-content__list">
   <? foreach ($catalog as $key => $items): ?>
     <div class="catalog-content__list__tab <? echo ($key == 0 ? 'active' : ''); ?>">
       <? foreach ($items['items'] as $key => $item): ?>
       <? $index = (int)$key + 1; ?>
       <? if($index == 1 || ($index - 1) % 16 == 0): ?>
       <div class="catalog-content__list__inner <? echo ($index == 1 ? 'active' : ''); ?>">
       <? endif; ?>
           <div class="catalog-content__list__item">
             <div class="catalog-content__list__image">
               <img src="<? echo gi($item['image'], 'catalog_img'); ?>" alt="<? echo $item['title']; ?>">
             </div>
             <div class="catalog-content__list__title"><? echo $item['title']; ?></div>
             <div class="catalog-content__list__price">
               <span class="new"><? echo $item['new_price']; ?> руб./м²</span><span class="old"><? echo $item['old_price']; ?> руб./м²</span>
             </div>
             <a href="" class="catalog-content__list__button toModal" data-modal="modal-typical" data-modal-title="<? echo $modal_catalog_title; ?>" data-modal-ya="<? echo $modal_catalog_target; ?>"><span>оставить заявку</span></a>
           </div>
         <? if($index % 16 == 0 || $index == count($items['items'])): ?>
         </div>
         <? endif; ?>
         <? endforeach; ?>
      <div class="catalog-content__pagination">
       <?
       $total = count($items['items']) / 16;
       if(count($items['items']) % 16 != 0){
        $total++;
       }
       ?>
       <? for($i = 1; $i < $total; $i++): ?>
        <div class="catalog-content__pagination__item <? echo ($i == 1 ? 'active' : ''); ?>"><? echo $i; ?></div>
        <? endfor; ?>
      </div>
     </div>
   <? endforeach; ?>
  </div>
 </div>
</section>
<section class="catalog-order" style="background: url(<? echo gi($fields['form_background'], 'full'); ?>) no-repeat center; background-size: cover">
 <div class="container">
  <div class="catalog-order__title">
   Заявка на расчет стоимости<br />
   напольных материалов
  </div>
  <div class="catalog-order__content">
   <? echo do_shortcode('[contact-form-7 id="209" title="Открытая форма"]'); ?>
   <div class="catalog-order__separator"></div>
   <div class="catalog-order__list">
    <div class="catalog-order__list__item">
     <div class="catalog-order__list__icon">
      <svg>
       <use xlink:href="#circle-check"></use>
      </svg>
     </div>
     <div class="catalog-order__list__text">
      Бесплатная доставка<br />
      по Пензе
     </div>
    </div>
    <div class="catalog-order__list__item">
     <div class="catalog-order__list__icon">
      <svg>
       <use xlink:href="#circle-check"></use>
      </svg>
     </div>
     <div class="catalog-order__list__text">
      Гарантия низкой цены<br />
      на материалы
     </div>
    </div>
    <div class="catalog-order__list__item">
     <div class="catalog-order__list__icon">
      <svg>
       <use xlink:href="#circle-check"></use>
      </svg>
     </div>
     <div class="catalog-order__list__text">
      Бесплатная разгрузка<br />
      и подъем на этаж
     </div>
    </div>
    <div class="catalog-order__list__item">
     <div class="catalog-order__list__icon">
      <svg>
       <use xlink:href="#circle-check"></use>
      </svg>
     </div>
     <div class="catalog-order__list__text">
      Дополнительная скидка на<br />
      профессиональную укладку
     </div>
    </div>
   </div>
  </div>
 </div>
</section>
<?php get_footer(); ?>
