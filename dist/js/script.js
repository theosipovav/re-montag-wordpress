const $ = jQuery;

$(document).ready(function () {
    const targetSelectMaterial = 'select[name=material]';
    const targetInputDiameter = 'input[name=diameter]';
    const targetInputDepth = 'input[name=depth]';
    const targetInputNumber = 'input[name=number]';
    const targetHiddenPrice = 'input[name=hidden_price]';
    const targetHiddenMaterial = 'input[name=hidden_material]';
    const targetHiddenDiameter = 'input[name=hidden_diameter]';
    const targetHiddenDepth = 'input[name=hidden_depth]';
    const targetHiddenNumber = 'input[name=hidden_number]';

    /**
     * 
     * @returns 
     */
    function updateCalcAlmazBur() {

        var valueMaterial = $(targetSelectMaterial).val();
        var valueDiameter = $(targetInputDiameter).val();
        var valueDepth = $(targetInputDepth).val();
        var valueNumber = $(targetInputNumber).val();
        var price = 0;





        //
        // Обработка ошибок
        var isError = checkInputFormCalculateAlmazbur();
        if (isError) {
            $(targetHiddenPrice).val(0);
            return;
        }
        $(".form-calc-almazbur-result").addClass("load");
        var action = $("#UrlAdminAjax").val() + "/?action=calc_almazbur";
        $.ajax({
            type: "POST",
            url: action,
            data: {
                action: 'calc_almazbur',
                material: valueMaterial,
                diameter: valueDiameter,
                depth: valueDepth,
                number: valueNumber,
            },
            dataType: "text",
            success: function (response) {
                $("span.form-calc-almazbur-result-price").html(response);
                $(targetHiddenPrice).val(response);
                $(targetHiddenMaterial).val(valueMaterial);
                $(targetHiddenDiameter).val(valueDiameter);
                $(targetHiddenDepth).val(valueDepth);
                $(targetHiddenNumber).val(valueNumber);
                $(".form-calc-almazbur-result").removeClass("load");
            },
            error: function (a, b, c) {
                console.log(a);
                console.log(b);
                console.log(c);
            }
        });


    }

    function checkInputFormCalculateAlmazbur() {
        var valueMaterial = $(targetSelectMaterial).val();
        var valueDiameter = $(targetInputDiameter).val();
        var valueDepth = $(targetInputDepth).val();
        var valueNumber = $(targetInputNumber).val();

        var isError = false;
        if (valueMaterial == "" || valueMaterial == null) {
            $(targetSelectMaterial).addClass("error");
            isError = true;
        } else {
            $(targetSelectMaterial).removeClass("error");
        }
        if (valueDiameter == "" || valueDiameter == 0) {
            $(targetInputDiameter).addClass("error");
            isError = true;
        } else {
            $(targetInputDiameter).removeClass("error");
        }
        if (valueDepth == "" || valueDepth == 0) {
            $(targetInputDepth).addClass("error");
            isError = true;
        } else {
            $(targetInputDepth).removeClass("error");
        }
        if (valueNumber == "" || valueNumber == 0) {
            $(targetInputNumber).addClass("error");
            isError = true;
        } else {
            $(targetInputNumber).removeClass("error");
        }
        return isError;
    }

    $(targetSelectMaterial).on('change', function () {
        updateCalcAlmazBur();
    });
    $(targetInputDiameter).on('change', function () {
        updateCalcAlmazBur();
    });
    $(targetInputDepth).on('change', function () {
        updateCalcAlmazBur();
    });
    $(targetInputNumber).on('change', function () {
        updateCalcAlmazBur();
    });





});