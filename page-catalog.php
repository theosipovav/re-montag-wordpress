<?php
/*
Template Name: Catalog
*/
get_header();
?>
<header class="category-header">
  <div class="container">
    <h1 class="category-header__title">
      Распродажа напольных<br />
      покрытий от производителей<br />
      <span>со склада в Пензе</span>
    </h1>
    <div class="category-header__tags">
      <div class="category-header__tags__item">Ламинат</div>
      <div class="category-header__tags__item">Паркет</div>
      <div class="category-header__tags__item">ПВХ-пленка</div>
      <div class="category-header__tags__item">Пробковый пол</div>
    </div>
    <div class="category-header__bottom">
      <a href="" class="category-header__button toModal" data-modal="modal-typical" data-modal-title="Рассчитать стоимость напольных покрытий" data-modal-ya="raschet">
        <span>Рассчитать стоимость</span>
      </a>
      <div class="category-header__note">
        Оставьте заявку на сайте и сравните наши цены<br />
        с ценами в магазинах. Убедитесь лично в более низкой<br />
        стоимости за те же напольные покрытия.
      </div>
    </div>
    <div class="category-header__list">
      <div class="category-header__list__item">
        <div class="category-header__list__icon">
          <svg>
            <use xlink:href="#funds"></use>
          </svg>
        </div>
        <div class="category-header__list__text">
          Цена на 25-30%<br />
          ниже чем в магазинах<br />
          стройматериалов<br />
          по городу
        </div>
      </div>
      <div class="category-header__list__item">
        <div class="category-header__list__icon">
          <svg>
            <use xlink:href="#verification-of-delivery-list-clipboard-symbol"></use>
          </svg>
        </div>
        <div class="category-header__list__text">
          Бесплатная доставка<br />
          по Пензе и подъем<br />
          на этаж
        </div>
      </div>
      <div class="category-header__list__item">
        <div class="category-header__list__icon">
          <svg>
            <use xlink:href="#surface1"></use>
          </svg>
        </div>
        <div class="category-header__list__text">
          Гарантия низкой<br />
          цены
        </div>
      </div>
      <div class="category-header__list__item">
        <div class="category-header__list__icon">
          <svg>
            <use xlink:href="#wrench"></use>
          </svg>
        </div>
        <div class="category-header__list__text">
          Известные<br />
          производители из<br />
          Германии, России,<br />
          Бельгии
        </div>
      </div>
    </div>
  </div>
</header>
<section class="category-catalog">
  <div class="container">
    <div class="category-catalog__title">
      Каталог товаров
    </div>
    <div class="category-catalog__list">
      <div class="category-catalog__list__item">
        <a href="<? the_permalink(11); ?>" class="">
          <div class="category-catalog__list__image">
            <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-catalog1.png" alt="">
          </div>
          <div class="category-catalog__list__bottom">
            <div class="category-catalog__list__title">
              Ламинат
            </div>
            <a href="<? the_permalink(11); ?>" class="category-catalog__list__button">
              Подробнее
              <svg>
                <use xlink:href="#button-arrow"></use>
              </svg>
            </a>
          </div>
        </a>
      </div><!-- category-catalog__list__item -->
      <div class="category-catalog__list__item">
        <a href="<? the_permalink(37); ?>" class="">
          <div class="category-catalog__list__image">
            <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-catalog2.png" alt="">
          </div>
          <div class="category-catalog__list__bottom">
            <div class="category-catalog__list__title">
              Паркет
            </div>
            <a href="<? the_permalink(37); ?>" class="category-catalog__list__button">
              Подробнее
              <svg>
                <use xlink:href="#button-arrow"></use>
              </svg>
            </a>
          </div>
        </a>
      </div><!-- category-catalog__list__item -->
      <div class="category-catalog__list__item">
        <a href="<? the_permalink(55); ?>" class="">
          <div class="category-catalog__list__image">
            <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-catalog3.png" alt="">
          </div>
          <div class="category-catalog__list__bottom">
            <div class="category-catalog__list__title">
              Пробковый пол
            </div>
            <a href="<? the_permalink(55); ?>" class="category-catalog__list__button">
              Подробнее
              <svg>
                <use xlink:href="#button-arrow"></use>
              </svg>
            </a>
          </div>
        </a>
      </div><!-- category-catalog__list__item -->
      <div class="category-catalog__list__item">
        <a href="<? the_permalink(49); ?>" class="">
          <div class="category-catalog__list__image">
            <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-catalog4.png" alt="">
          </div>
          <div class="category-catalog__list__bottom">
            <div class="category-catalog__list__title">
              ПВХ-плитка
            </div>
            <a href="<? the_permalink(49); ?>" class="category-catalog__list__button">
              Подробнее
              <svg>
                <use xlink:href="#button-arrow"></use>
              </svg>
            </a>
          </div>
        </a>
      </div><!-- category-catalog__list__item -->
    </div>
  </div>
</section>
<section class="category-related">
  <div class="container">
    <div class="category-related__title">Сопутствующие товары</div>
    <div class="category-related__list">
      <div class="category-related__list__item">
        <div class="category-related__list__image">
          <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-related1.png" alt="">
        </div>
        <div class="category-related__list__desc">
          Плинтус
        </div>
      </div>
      <div class="category-related__list__item">
        <div class="category-related__list__image">
          <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-related2.png" alt="">
        </div>
        <div class="category-related__list__desc">
          Подложка
        </div>
      </div>
      <div class="category-related__list__item">
        <div class="category-related__list__image">
          <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-related3.png" alt="">
        </div>
        <div class="category-related__list__desc">
          Пороги
        </div>
      </div>
      <div class="category-related__list__item">
        <div class="category-related__list__image">
          <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-related4.png" alt="">
        </div>
        <div class="category-related__list__desc">
          Защита и уход
        </div>
      </div>
      <div class="category-related__list__item">
        <div class="category-related__list__image">
          <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-related5.png" alt="">
        </div>
        <div class="category-related__list__desc">
          Клей
        </div>
      </div>
      <div class="category-related__list__item">
        <div class="category-related__list__image">
          <img src="<? echo get_template_directory_uri(); ?>/dist/img/category-related6.png" alt="">
        </div>
        <div class="category-related__list__desc">
          Химия для укладки
        </div>
      </div>
    </div>
  </div>
</section>
<section class="catalog-order">
  <div class="container">
    <div class="catalog-order__title">
      Заявка на расчет стоимости<br />
      напольных материалов
    </div>
    <div class="catalog-order__content">
      <? echo do_shortcode('[contact-form-7 id="209" title="Открытая форма"]'); ?>
      <div class="catalog-order__separator"></div>
      <div class="catalog-order__list">
        <div class="catalog-order__list__item">
          <div class="catalog-order__list__icon">
            <svg>
              <use xlink:href="#circle-check"></use>
            </svg>
          </div>
          <div class="catalog-order__list__text">
            Бесплатная доставка<br />
            по Пензе
          </div>
        </div>
        <div class="catalog-order__list__item">
          <div class="catalog-order__list__icon">
            <svg>
              <use xlink:href="#circle-check"></use>
            </svg>
          </div>
          <div class="catalog-order__list__text">
            Гарантия низкой цены<br />
            на материалы
          </div>
        </div>
        <div class="catalog-order__list__item">
          <div class="catalog-order__list__icon">
            <svg>
              <use xlink:href="#circle-check"></use>
            </svg>
          </div>
          <div class="catalog-order__list__text">
            Бесплатная разгрузка<br />
            и подъем на этаж
          </div>
        </div>
        <div class="catalog-order__list__item">
          <div class="catalog-order__list__icon">
            <svg>
              <use xlink:href="#circle-check"></use>
            </svg>
          </div>
          <div class="catalog-order__list__text">
            Дополнительная скидка на<br />
            профессиональную укладку
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>