<?php
/*
Template Name: AlmazBur
*/
get_header();


$sectionAlmazburInfo = get_field('content');
$sectionAlmazburForm = get_field('form');
$sectionAlmazburInfo2 = get_field('info');

?>
<header class="home-header" style="background-image: url(<?= get_field('background_img')['url'] ?>);">
    <div class="container">
        <div class="d-flex flex-column justify-content-end mt-auto">
            <h1 class="home-header__title">
                <?= get_field('title') ?>
            </h1>
            <div class="d-flex justify-content-start align-items-center">
                <a href="" class="btn-intro toModal" data-modal="modal-almazbur" data-modal-title="<?= $sectionAlmazburForm['title']  ?>" data-modal-ya="dizain">
                    <span>Отправить запрос</span>
                </a>
            </div>
            <div class="header-list">
                <div class="header-list-item">
                    <div class="header-list-ico"><img src="<?= get_template_directory_uri()  ?>/dist/img/list-hero-1.png" alt=""></div>
                    <p class="header-list--text">Низкие цены</p>
                </div>
                <div class="header-list-item">
                    <div class="header-list-ico"><img src="<?= get_template_directory_uri()  ?>/dist/img/list-hero-2.png" alt=""></div>
                    <p class="header-list--text">Без предоплат</p>
                </div>
                <div class="header-list-item">
                    <div class="header-list-ico"><img src="<?= get_template_directory_uri()  ?>/dist/img/list-hero-3.png" alt=""></div>
                    <p class="header-list--text">Гарантия качества</p>
                </div>
                <div class="header-list-item">
                    <div class="header-list-ico"><img src="<?= get_template_directory_uri()  ?>/dist/img/list-hero-4.png" alt=""></div>
                    <p class="header-list--text">Оперативный выезд</p>
                </div>
                <div class="header-list-item">
                    <div class="header-list-ico"><img src="<?= get_template_directory_uri()  ?>/dist/img/list-hero-5.png" alt=""></div>
                    <p class="header-list--text">Работаем без выходных</p>
                </div>
            </div>
        </div>

        <div class="home-header__scroll scrollTo" data-target="home-catalog">
            <svg>
                <use xlink:href="#arrow-down"></use>
            </svg>
        </div>
    </div>
</header>
<section class="section almazbur-info">
    <div class="container">
        <div class="d-flex flex-column">
            <h2 class="section-title almazbur-info-title"><?= $sectionAlmazburInfo['title'] ?></h2>
            <div class="almazbur-info-container">
                <div class="almazbur-info-text">
                    <?= $sectionAlmazburInfo['text'] ?>
                </div>
                <div class="almazbur-info-img">
                    <?= wp_get_attachment_image($sectionAlmazburInfo['img']['ID'], 'large'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="section almazbur-form">
    <div class="container">
        <div class="d-flex flex-column">
            <div class="section-title almazbur-form-title">
                <?= $sectionAlmazburForm['title'] ?>
            </div>
            <div class="almazbur-form-form">
                <?= do_shortcode($sectionAlmazburForm['code']); ?>
            </div>
        </div>
    </div>
</div>
<section class="section almazbur-info">
    <div class="container">
        <div class="d-flex flex-column">
            <h2 class="section-title almazbur-info-title"><?= $sectionAlmazburInfo2['title'] ?></h2>
            <div class="almazbur-info-container">
                <div class="almazbur-info-img">
                    <?= wp_get_attachment_image($sectionAlmazburInfo2['img']['ID'], 'large'); ?>
                </div>
                <div class="almazbur-info-text almazbur-info-text-right">
                    <?= $sectionAlmazburInfo2['text'] ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?= renderSectionPriceTableForAlmazbur() ?>
<?= renderSectionFormZamerForAlmazbur() ?>
<?= renderSectionBenefits() ?>
<?php get_footer(); ?>