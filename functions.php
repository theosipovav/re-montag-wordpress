<?php

/**
 * Отладка. Вывод информации по переменной - в.1
 */
function d($value)
{
  echo '<div class="" style="font-size: 16px;">';
  echo "<pre>" . print_r($value, true) . "</pre>";
  echo '</div>';
}
/**
 * Отладка. Вывод информации по переменной - в.2
 */
function dd($value)
{
  echo '<div class="" style="font-size: 16px;">';
  echo "<pre>" . print_r($value, true) . "</pre>";
  echo '</div>';
  exit();
}
/**
 * Отладка. Вывод информации по переменной - в.3
 */
function ddd($value)
{
  echo "<pre>" . htmlentities(print_r($value, true)) . "</pre>";
  exit();
}


function lp_styles()
{
  wp_enqueue_style('d_vendors', get_template_directory_uri() . '/dist/css/vendors.css');
  wp_enqueue_style('d_app', get_template_directory_uri() . '/dist/css/app.css');
  wp_enqueue_script('yandex-maps', '//api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU', array(), null, true);
  wp_deregister_script('jquery');
  wp_register_script('jquery', get_template_directory_uri() . '/dist/js/jquery-2.2.4.min.js');
  wp_enqueue_script('jquery');
  wp_enqueue_script('u_vendors', get_template_directory_uri() . '/dist/js/vendors.js', array(), null, true);
  wp_enqueue_script('u_app', get_template_directory_uri() . '/dist/js/app.js', array(), null, true);
  wp_enqueue_script('script', get_template_directory_uri() . '/dist/js/script.js', array('jquery'), null, true);
  wp_enqueue_style('style', get_template_directory_uri() . '/dist/css/style.min.css');
}
add_action('wp_enqueue_scripts', 'lp_styles');
/**
 * Добавляет секции, параметры и элементы управления (контролы) на страницу настройки темы
 */
function lading_customize_register($customize)
{
  $customize->add_panel(
    'setting-panel',
    array(
      'priority' => 10,
      'capability' => 'edit_theme_options',
      'title' => 'Настройка шаблона',
    )
  );
  /**
   *
   * Первый экран
   **/
  $customize->add_section(
    'setting',
    array(
      'title' => 'Основные настройки',
      'priority' => 35,
      'panel' => 'setting-panel',
    )
  );
  $customize->add_setting(
    'setting-phone',
    array(
      'default' => '+7 999 999 99 99'
    )
  );
  $customize->add_control(
    'setting-phone',
    array(
      'label' => 'Номер телефона',
      'section' => 'setting',
      'type' => 'text'
    )
  );
  $customize->add_setting(
    'setting-email',
    array(
      'default' => 'example@example.com'
    )
  );
  $customize->add_control(
    'setting-email',
    array(
      'label' => 'E-mail',
      'section' => 'setting',
      'type' => 'text'
    )
  );
  $customize->add_setting(
    'setting-inst',
    array(
      'default' => 'http://instagram.com'
    )
  );
  $customize->add_control(
    'setting-inst',
    array(
      'label' => 'Inst',
      'section' => 'setting',
      'type' => 'text'
    )
  );
  $customize->add_setting(
    'setting-you',
    array(
      'default' => 'http://youtube.com'
    )
  );
  $customize->add_control(
    'setting-you',
    array(
      'label' => 'Youtube',
      'section' => 'setting',
      'type' => 'text'
    )
  );
  $customize->add_setting(
    'setting-politic'
  );
  $customize->add_control(
    new WP_Customize_Upload_Control(
      $customize,
      'setting-politic',
      array(
        'label' => 'Политика конфиденциальности',
        'section' => 'setting',
        'settings' => 'setting-politic'
      )
    )
  );
  $customize->add_setting(
    'analytics',
    array(
      'default' => ''
    )
  );
  $customize->add_control(
    'analytics',
    array(
      'label' => 'Код аналитики',
      'section' => 'setting',
      'type' => 'textarea'
    )
  );
  /* Option list of all post */
  $construction_landing_page_options_posts = array();
  $construction_landing_page_options_posts_obj = get_posts('posts_per_page=-1');
  $construction_landing_page_options_posts[''] = __('Выберите пост', 'construction-landing-page');
  foreach ($construction_landing_page_options_posts_obj as $posts) {
    $construction_landing_page_options_posts[$posts->ID] = $posts->post_title;
  }
  /* Option list of all post/page */
  $construction_landing_page_options_posts_pages     = array();
  $construction_landing_page_options_posts_pages_obj = get_posts(array('posts_per_page' => '-1', 'post_type' => array('page', 'post')));
  $construction_landing_page_options_posts_pages[''] = __('Выберите пост/страницу', 'construction-landing-page');
  foreach ($construction_landing_page_options_posts_pages_obj as $construction_landing_page_posts) {
    $construction_landing_page_options_posts_pages[$construction_landing_page_posts->ID] = $construction_landing_page_posts->post_title;
  }
  /* Option list of all page */
  $construction_landing_page_options_pages     = array();
  $construction_landing_page_options_pages_obj = get_posts('post_type=page&posts_per_page=-1');
  $construction_landing_page_options_pages[''] = __('Выберите страницу', 'construction-landing-page');
  foreach ($construction_landing_page_options_pages_obj as $construction_landing_page_pages) {
    $construction_landing_page_options_pages[$construction_landing_page_pages->ID] = $construction_landing_page_pages->post_title;
  }
  function construction_landing_page_sanitize_select($input, $setting)
  {
    // Ensure input is a slug.
    $input = sanitize_key($input);
    // Get list of choices from the control associated with the setting.
    $choices = $setting->manager->get_control($setting->id)->choices;
    // If the input is a valid key, return it; otherwise, return the default.
    return (array_key_exists($input, $choices) ? $input : $setting->default);
  }
}
function getCategoryList($parentId = 5)
{
  $args = array(
    'child_of' => $parentId,
    'hide_empty' => 1
  );
  return get_categories($args);
}
function get_image($name, $w)
{
  $image_url = get_theme_mod($name);
  $image_id = attachment_url_to_postid($image_url);
  if ($image_id) {
    $image_url = wp_get_attachment_image($image_id, $w);
  }
  return $image_url;
}
if (function_exists('add_theme_support')) {
  add_theme_support('menus');
}
register_nav_menus(array(
  'footer1' => 'Меню в подвале1',
  'footer2' => 'Меню в подвале2',
  'footer3' => 'Меню в подвале3',
  'header' => 'Меню в шапке',
  'pages' => 'Меню на страницах'
));
//add_filter('wpcf7_form_elements', function($content) {
//  $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
//  return $content;
//});

add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);
function special_nav_class($classes, $item)
{
  if ($item->title == "Каталог" && in_category('catalog')) {
    $classes[] = "current-menu-item";
  }
  return $classes;
}
function new_excerpt_length($length)
{
  return 20;
}
function gi($id, $size)
{
  return wp_get_attachment_image_src($id, $size)[0];
}
add_filter('excerpt_length', 'new_excerpt_length');
add_filter('excerpt_more', function ($more) {
  return '..';
});
add_action('customize_register', 'lading_customize_register');
add_theme_support('post-thumbnails');
add_image_size('brand_img', 170, 58, true);
add_image_size('catalog_img', 270, 270, true);
add_image_size('slider_img', 570, 313, true);
function number2word($n, $titles)
{
  $cases = array(2, 0, 1, 1, 1, 2);
  return $titles[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
}
function wpd_subcategory_template($template)
{
  $cat = get_queried_object();
  if (0 < $cat->category_parent)
    $template = locate_template('category-for-girls.php');
  return $template;
}
//add_filter( 'category_template', 'wpd_subcategory_template' );
add_filter('pre_get_posts', function ($query) {
  if (isset($_GET['cat']) && $_GET['source'] != '') {
    $query->set('cat', array(
      $_GET['cat']
    ));
  }
});
add_action('init', 'rb_session_start', 1);
function rb_session_start()
{
  session_start();
}
function saveUtm()
{
  if (isset($_REQUEST['utm_source'])) {
    $_SESSION['utm_source'] = $_REQUEST['utm_source'];
  }
  if (isset($_REQUEST['utm_medium'])) {
    $_SESSION['utm_medium'] = $_REQUEST['utm_medium'];
  }
  if (isset($_REQUEST['utm_campaign'])) {
    $_SESSION['utm_campaign'] = $_REQUEST['utm_campaign'];
  }
  if (isset($_REQUEST['utm_content'])) {
    $_SESSION['utm_content'] = $_REQUEST['utm_content'];
  }
  if (isset($_REQUEST['utm_term'])) {
    $_SESSION['utm_term'] = $_REQUEST['utm_term'];
  }
  if (isset($_REQUEST['utm_position_type'])) {
    $_SESSION['utm_position_type'] = $_REQUEST['utm_position_type'];
  }
  if (isset($_REQUEST['utm_position'])) {
    $_SESSION['utm_position'] = $_REQUEST['utm_position'];
  }
  if (isset($_REQUEST['utm_placement'])) {
    $_SESSION['utm_placement'] = $_REQUEST['utm_placement'];
  }
}
add_action('init', 'saveUtm');
function writeUtm()
{
  foreach ($_SESSION as $key => $value) {
    echo "<input type=\"hidden\" name='$key' value=\"$value\">";
  }
}
add_action('get_footer', 'writeUtm');



include_once 'functions-ajax.php';





function renderSectionPriceTableForAlmazbur()
{
  $dataSection = get_field('price');
  $dataCalc = array();
  if (is_array($dataSection['table'])) {
    foreach ($dataSection['table'] as $num => $row) {
      //
    }
  }
  ob_start();
  include(locate_template('templates/section-almazbur-price.php'));
  $out = ob_get_contents();
  ob_end_clean();
  return $out;
}



function renderSectionBenefits()
{
  ob_start();
  include(locate_template('templates/section-benefits.php'));
  $out = ob_get_contents();
  ob_end_clean();
  return $out;
}

function renderSectionFormZamerForAlmazbur()
{
  ob_start();
  include(locate_template('templates/section-zamer-almazbur.php'));
  $out = ob_get_contents();
  ob_end_clean();
  return $out;
}
