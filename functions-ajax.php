<?php




function ajaxCalcAlmazbur()
{
    $res = 0;
    $data = $_POST;
    $currentDiameter = intval($data['diameter']);
    $currentMaterial = intval($data['material']);
    $dataSection = get_field('price', 504);
    if (is_array($dataSection['table'])) {
        $rowCurrent = false;
        foreach ($dataSection['table'] as $num => $row) {
            $diameter = $row['d'];
            if (strpos(mb_strtoupper($diameter), "ДО") !== false) {
                $diameter = preg_replace("/[^,.0-9]/", '', $diameter);
                $diameter = intval($diameter);
                if ($currentDiameter <= $diameter) {
                    $rowCurrent = $row;
                    break;
                }
            }
            if (strpos($diameter, '-') !== false) {
                $arr = explode('-', $diameter);
                $diameterDown = intval($arr[0]);
                $diameterUp = intval($arr[1]);
                if ($currentDiameter >= $diameterDown && $currentDiameter <= $diameterUp) {
                    $rowCurrent = $row;
                    break;
                }
            }
            $diameter = intval($diameter);
            if ($currentDiameter == $diameter) {
                $rowCurrent = $row;
                break;
            }
            if ($currentDiameter < $diameter) {
                $rowCurrent = $row;
                break;
            }
        }
    }
    $currentMaterial = $data['material'];
    $priceMaterial = intval($rowCurrent[$currentMaterial]);
    $depth = $data['depth'];
    $count = $data['number'];
    $res = $priceMaterial * $depth * $count;
    print_r($res);
    die;
}
add_action('wp_ajax_calc_almazbur', 'ajaxCalcAlmazbur');
add_action('wp_ajax_nopriv_calc_almazbur', 'ajaxCalcAlmazbur');
