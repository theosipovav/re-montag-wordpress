(() => {
  function requireAll(r) {
    r.keys().forEach(r);
  }
  requireAll(require.context("../images/sprites/", true, /\.svg$/));
  lazyload();
  function modalClose(){
    $('.modal__close, .modal__fade').on('click', function(){
      if($(this).parents('.modal').hasClass('modal-politic')){
        $('.modal-politic').fadeOut(500);
      }else {
        $('.modal').fadeOut(500);
      }
    })
  }
  modalClose();
  function tabs(){
    $('.catalog-content__cats').on('click', '.catalog-content__cats__item:not(.active)', function() {
      $(this).addClass('active').siblings().removeClass('active');
      $('.catalog-content__list').find('.catalog-content__list__tab').removeClass('active').eq($(this).index()).addClass('active');
      return false;
    });
  }
  tabs();
  function showModal(){
    $('.toModal').on('click', function(){
      let target = $(this).attr('data-modal');
      let title = $(this).attr('data-modal-title');
      if(title){
        $('.'+target).find('.modal__title').html(title);
        $('.'+target).find('[name=form_title]').val(title);
      }
      $('.'+target).fadeIn(500);
      return false;
    })
  }
  showModal();
  function menu(){
    $('.menu__open').click(function () {
      $('.menu').addClass('active');
    })
    $('.menu__close').click(function () {
      $('.menu').removeClass('active');
    })
  }
  menu();

  function scrollTo() {
    $(".scrollTo").click(function() {
      let target = '.' + $(this).attr('data-target');
      $('.menu').removeClass('active');
      if(document.location.pathname == "/"){
        $('html').animate({
          scrollTop: $(target).offset().top
        }, 500);
        return !1;
      }
    });
  }
  scrollTo();
  if(document.getElementById('map')) {
    ymaps.ready(function () {
      setMap();
    });
  }
  function setMap() {
    let myMap1;
    myMap1 = new ymaps.Map("map", {
      center: [53.188444, 45.037621],
      zoom: 17,
      controls: ['zoomControl']
    });
    myMap1.behaviors.disable('scrollZoom');
    var myPlacemark = new ymaps.Placemark(myMap1.getCenter());
    myMap1.geoObjects.add(myPlacemark);
  }
  function setGallery() {
    $('.home-projects__list__slider__inner').each(function (idx, elem) {
      const parent = $(elem).parents('.home-projects__list__slider');
      const prev = parent.find('.home-projects__list__slider__control:nth-child(1)');
      const next = parent.find('.home-projects__list__slider__control:nth-child(2)');
      const dots = $(parent).find('.home-projects__list__slider__dots');
      const slider = $(elem);
      slider.owlCarousel({
        margin: 0,
        dots: true,
        dotsContainer: dots,
        items: 1,
        center: true,
        loop: true
      });
      prev.click(function () {
        slider.trigger('prev.owl.carousel', [500]);
      })
      next.click(function () {
        slider.trigger('next.owl.carousel', [500]);
      })
    })
    const prev = $('.home-production__slider__control:nth-child(1)');
    const next = $('.home-production__slider__control:nth-child(2)');
    const slider = $('.home-production__slider__inner');
    slider.owlCarousel({
      margin: 8,
      dots: false,
      responsive: {
        0: {
          slideBy: 1,
          items: 1
        },
        768: {
          items: 2
        },
        970: {
          items: 3
        },
        1380: {
          items: 3,
        }
      }
    });
    prev.click(function () {
      slider.trigger('prev.owl.carousel', [500]);
    })
    next.click(function () {
      slider.trigger('next.owl.carousel', [500]);
    })
  }

  setGallery();
})();
